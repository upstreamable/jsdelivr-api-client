<?php
$finder = PhpCsFixer\Finder::create()
    ->name('*.php')
    ->in(__DIR__ . '/src')
;
$config = new PhpCsFixer\Config();

return $config->setRules([
        '@PSR12' => true,
        'linebreak_after_opening_tag' => true,
        'ordered_imports' => true,
        'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
;
